#!flask/bin/python3
from flask import Flask, jsonify, request, abort
from lib.clade import Clade
from lib.most_helper import MostHelper
from lib.news_helper import NewsHelper
from lib.dividend_helper import DividendHelper
from lib.company_fundamentals_helper import CompanyFundamentalsHelper
from lib.company_details_helper import CompanyDetailsHelper
from lib.markets_helper import MarketsHelper

from lib.ticker_helper import TickerHelper

app = Flask(__name__)
db = Clade().db


@app.route('/news', methods=['GET'])
def news():
    return NewsHelper.get_news(request, db)


@app.route('/news_headlines', methods=['GET'])
def news_headlines():
    return NewsHelper.get_headlines(db)


@app.route('/dividends', methods=['GET'])
def dividends():
    return DividendHelper.get_dividends(request, db)


@app.route('/company', methods=['GET'])
def company():
    return CompanyFundamentalsHelper.get_company_fundamentals(request, db)


@app.route('/company_details', methods=['GET'])
def company_details():
    return CompanyDetailsHelper.get_company_details(request, db)


@app.route('/company_details_industry', methods=['GET'])
def get_all_companies_by_industry():
    return CompanyDetailsHelper.get_all_companies_by_industry(db)


@app.route('/companies_by_industry', methods=['GET'])
def companies_by_industry():
    return CompanyDetailsHelper.get_companies_by_industry(request, db)


@app.route('/most', methods=['GET'])
def most():
    return MostHelper.get(db)


@app.route('/markets', methods=['GET'])
def markets():
    return MarketsHelper.get(db)


@app.route('/all_companies', methods=['GET'])
def all_companies():
    return CompanyDetailsHelper.get_all_companies(db)


@app.route('/companies_by_symbol', methods=['GET'])
def companies_by_symbol():
    return CompanyDetailsHelper.get_companies_by_symbol(request, db)


@app.route('/similar_companies', methods=['GET'])
def similar_companies():
    return CompanyDetailsHelper.get_similar(request, db)


@app.route('/companies_wish_list', methods=['GET'])
def companies_wish_list():
    return CompanyDetailsHelper.get_companies_wish_list(request, db)


@app.route('/ticker', methods=['GET'])
def ticker():
    return TickerHelper.get_ticker(request, db)


@app.route('/ticker_previous_day', methods=['GET'])
def ticker_previous_day():
    return TickerHelper.get_previous_day(request, db)

