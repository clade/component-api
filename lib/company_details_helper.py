from flask import abort
from bson.json_util import dumps


class CompanyDetailsHelper:

    @staticmethod
    def get_company_details(request, db):
        symbol = request.args.get('symbol')
        if symbol is None:
            abort(400, description="Symbol not found")

        company_details = db.company_data.find_one({'symbol': symbol})
        if company_details is None:
            abort(404, f"Symbol {symbol} not found")
        return dumps(company_details)

    @staticmethod
    def get_all_companies_by_industry(db):
        industry_with_counts = db.company_data.aggregate([
            {"$group": {
                "_id": "$industry",
                "count": {"$sum": 1}
            }},
            {"$group": {
                "_id": None,
                "counts": {
                    "$push": {"k": "$_id", "v": "$count"}
                }
            }},
            {"$replaceRoot": {
                "newRoot": {"$arrayToObject": "$counts"}
            }}
        ])

        return dumps(industry_with_counts)

    @staticmethod
    def get_companies_by_industry(request, db):
        industry_id = request.args.get('industry_id')

        industry_id_match = {"$match": {"industry_id": int(industry_id)}}

        company_data_group = {"$lookup": {"from": "company_data",
                                          "localField": "symbol",
                                          "foreignField": "symbol",
                                          "as": "company_data"}}

        pipeline = [
            industry_id_match,
            company_data_group
        ]

        if industry_id is None:
            abort(400, description="Industry not found")

        company_details = db.company_data.aggregate(pipeline)
        if company_details is None:
            abort(404, f"Industry {industry_id} not found")
        return dumps(company_details)

    @staticmethod
    def get_all_companies(db):
        return dumps(db.company_data.find())


    @staticmethod
    def get_companies_by_symbol(request, db):
        symbols = request.args.get('symbols')

        if symbols is None:
            abort(400, description="Symbol not found")

        symbols_list = symbols.split(",")
        companies = db.company_data.find({"symbol": {"$in": symbols_list}})
        return dumps(companies)

    #todo: pipeline the request
    @staticmethod
    def get_similar(request, db):
        symbol = request.args.get('symbol')
        if symbol is None:
            abort(400, description="Symbol not found")

        company = db.company_data.find_one({"symbol": symbol})
        related = db.company_data.find({"industry_id": company['industry_id'], "symbol": {"$ne": symbol}})

        return dumps(related)

    # todo: pipeline/map the request/response
    @staticmethod
    def get_companies_wish_list(request, db):
        symbols = request.args.get('symbols')
        if symbols is None:
            abort(400, description="Symbols not found")

        symbols_list = symbols.split(",")
        companies = db.company_data.find({"symbol": {"$in": symbols_list}})
        ticker_response = []
        for symbol in symbols_list:
            ticker_response.append(db.time_series_day.find_one({"$query": {'symbol': symbol},
                                                                "$orderby": {"values.datetime": -1}}))

        response = {
            "companies": companies,
            "ticker": ticker_response
        }

        return dumps(response)
