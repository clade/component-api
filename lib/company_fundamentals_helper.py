from flask import abort
from bson.json_util import dumps


class CompanyFundamentalsHelper:

    @staticmethod
    def get_company_fundamentals(request, db):
        symbol = request.args.get('symbol')
        if symbol is None:
            abort(400, description="Symbol not found")

        company_fundamentals_data = db.company_fundamentals.find_one({'symbol': symbol})
        if company_fundamentals_data is None:
            abort(404, f"Symbol {symbol} not found")
        return dumps(company_fundamentals_data)
