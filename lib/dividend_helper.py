from flask import abort
from bson.json_util import dumps


class DividendHelper:

    @staticmethod
    def get_dividends(request, db):
        symbol = request.args.get('symbol')
        if symbol is None:
            abort(400, description="Symbol not found")

        dividend_data = db.dividends.find_one({'symbol': symbol})
        if dividend_data is None:
            abort(404, f"Symbol {symbol} not found")
        return dumps(dividend_data)
