from bson.json_util import dumps


class MarketsHelper:

    @staticmethod
    def get(db):

        pipeline = [{"$lookup": {"from": "market_data",
                                 "localField": "symbol",
                                 "foreignField": "symbol",
                                 "as": "market_data"}}]

        markets = db.markets.aggregate(pipeline)

        return dumps(markets)
