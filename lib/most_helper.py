from bson.json_util import dumps


class MostHelper:

    @staticmethod
    def get(db):

        pipeline = [{"$lookup": {"from": "company_data",
                                 "localField": "symbol",
                                 "foreignField": "symbol",
                                 "as": "company_data"}}]

        most_active = db.most_active.aggregate(pipeline)
        most_gains = db.most_gains.aggregate(pipeline)

        most = {
            "gains": most_gains,
            "active": most_active
        }
        return dumps(most)
