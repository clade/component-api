from datetime import datetime
from flask import abort
from bson.json_util import dumps
import pymongo


class NewsHelper:

    @staticmethod
    def get_headlines(db):
        return dumps(db.news_headlines.find())

    @staticmethod
    def get_news(request, db):
        news_data = []
        symbol = request.args.get('symbol')
        if symbol is None:
            abort(400, description="Symbol not found")

        limit = request.args.get('limit')
        if limit is None:
            limit = 10
        else:
            limit = int(limit)

        date = request.args.get('date')
        if date is None:
            news_data = db.news.find({'symbol': symbol}, limit=limit).sort([('published_at', pymongo.DESCENDING)])
            if news_data.count() == 0:
                abort(404, f"Symbol {symbol} not found")
        else:
            try:
                query_date = datetime.strptime(date, '%Y-%m-%d')
                news_data = db.news.find({'symbol': symbol, 'published_at': {'$gte': query_date}}, limit=10).sort(
                    [('published_at', pymongo.DESCENDING)])
            except ValueError as e:
                abort(400, description=f'Date format incorrect, {e}')

        return dumps(news_data)
