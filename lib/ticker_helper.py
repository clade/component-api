from flask import abort
from bson.json_util import dumps
import pymongo


class TickerHelper:

    @staticmethod
    def lookup_db_collection(db, ticker_type):
        if ticker_type == '30min':
            return db.time_series_min_30
        if ticker_type == '1day':
            return db.time_series_day
        if ticker_type == '1week':
            return db.time_series_week
        if ticker_type == '1month':
            return db.time_series_month
        else:
            abort(400, description=f"{ticker_type} ticker_type not found")

    @staticmethod
    def get_ticker(request, db):
        symbol = request.args.get('symbol')
        if symbol is None:
            abort(400, description="Symbol not found")

        ticker_type = request.args.get('ticker_type')
        if ticker_type is None:
            abort(400, description="Ticker Type not found")

        limit = request.args.get('limit')
        if limit is None:
            limit = 10
        else:
            limit = int(limit)

        collection = TickerHelper.lookup_db_collection(db, ticker_type)
        results = collection.find({'symbol': symbol}, limit=limit).sort([('values.datetime', pymongo.DESCENDING)])

        return dumps(results)

    @staticmethod
    def get_previous_day(request, db):
        symbol = request.args.get('symbol')
        if symbol is None:
            abort(400, description="Symbol not found")

        result = db.time_series_day.find_one({"$query": {'symbol': symbol}, "$orderby": {"values.datetime": -1}})

        return dumps(result)
